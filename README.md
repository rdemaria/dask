# Dask Environment

This repository includes the definition of a Dask environment.

It includes all the required definitions to run at scale in CERN's
Binder/Jupyter setup, including scaling out to a large compute cluster.

Some examples are included and you can rely on your home directory being
available once you launch the environment (button below).

[![Binder](https://binder.cern.ch/badge_logo.svg)](https://binder.cern.ch/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Fbinder%2Frepos%2Fdask/master)
[![Build Status](https://gitlab.cern.ch/binder/repos/dask/badges/master/pipeline.svg)](https://gitlab.cern.ch/binder/repos/dask/pipelines)

![Dask Dashboard](images/dask.png "Dask Workspace")

## Customizing

Fork this repository to customize the environment. Check the files under the
`binder` directory to see how it is defined, and try it out directly in [Binder](https://binder.cern.ch).

## Contributing

If you want to share your changes with others, open a Merge Request in this
repository and it will be reviewed.

