
<img src="images/dask-horizontal.svg" width="50%" align="right">

## Dask Environment

This environment provides all the required setup to use Dask - [defined
here](https://gitlab.cern.ch/binder/repos/dask).

You can either use the resources directly available in this Jupyter server,
with something like:
```python
from dask.distributed import Client, progress
client = Client(processes=False, threads_per_worker=4,
                n_workers=1, memory_limit='2GB')
client
```

Or you can rely on a remote compute environment that is directly available to
you from this setup. To enable this, check the tab with the Dask logo on the
left, and drag the cluster defined there as a new cell in your notebook.

### Storage

**IMPORTANT**: For now you need to kinit **before** navigating the left pane on
the left, so that your credentials are available - this requirement will go
away soon. Open a terminal navigating the menu File/New/Terminal and run:
```
kinit youruser@CERN.CH
```

Navigate the file browser on the left pane to find your home directory.

### Home

